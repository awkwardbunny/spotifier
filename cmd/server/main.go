package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/awkwardbunny/spotifier/pkg/spotifier"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file: %v\n", err)
	}

	db, _ := gorm.Open("sqlite3", os.Getenv("DB_FILE"))
	defer db.Close()

	s := spotifier.New(db)

	bind := fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT"))
	log.Printf("Starting server on %s", bind)
	http.ListenAndServe(bind, s.Router())
}
