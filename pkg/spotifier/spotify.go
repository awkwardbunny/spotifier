package spotifier

func (s *Spotifier) DoPlay(uid int) error {
	c := s.SpotifyApi[uid]
	return c.Play()
}

func (s *Spotifier) DoPause(uid int) error {
	c := s.SpotifyApi[uid]
	return c.Pause()
}
