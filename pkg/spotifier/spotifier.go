package spotifier

import (
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/zmb3/spotify"
)

type Spotifier struct {
	DB          *gorm.DB
	SpotifyAuth spotify.Authenticator
	SpotifyApi  map[int]spotify.Client
	JWTAuth     *jwtauth.JWTAuth
}

type Token struct {
	gorm.Model
	Token string `json:"token"`
	State string `json:"state"`
}

var scopes = []string{
	spotify.ScopeStreaming,
	spotify.ScopeUserReadPrivate,
	spotify.ScopeUserReadEmail,
	spotify.ScopeUserReadPlaybackState,
	spotify.ScopeUserModifyPlaybackState,
	spotify.ScopeUserLibraryRead,
	spotify.ScopeUserLibraryModify,
}

func New(database *gorm.DB) *Spotifier {

	database.AutoMigrate(&Token{})

	return &Spotifier{
		DB:          database,
		SpotifyApi:  make(map[int]spotify.Client),
		SpotifyAuth: spotify.NewAuthenticator(os.Getenv("REDIRECT_URL"), scopes...),
		JWTAuth:     jwtauth.New("HS256", []byte(os.Getenv("JWT_SECRET")), nil),
	}
}

func (s *Spotifier) Router() http.Handler {
	router := chi.NewRouter()
	router.Use(middleware.Logger)
	router.Use(middleware.Timeout(60 * time.Second))
	router.Mount("/api", s.APIRouter())
	return router
}
