package spotifier

import (
	"context"
	"encoding/json"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
)

type JSONError struct {
	Error string `json:"error"`
}

type JSONStatus struct {
	Status string `json:"status"`
}

func (s *Spotifier) APIContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Get uid from jwt
		_, claims, _ := jwtauth.FromContext(r.Context())
		uid := int(claims["user_id"].(float64))

		// Get token from uid
		// var t Token
		// s.DB.First(&t, uid)

		ctx := context.WithValue(r.Context(), "uid", uid)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (s *Spotifier) APIRouter() http.Handler {
	r := chi.NewRouter()

	r.Get("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})

	r.Get("/auth", func(w http.ResponseWriter, r *http.Request) {

		code := r.URL.Query().Get("code")

		// From app
		if code == "" {

			for _, c := range r.Cookies() {
				if c.Name == "jwt" {
					http.Redirect(w, r, "/", http.StatusMovedPermanently)
					return
				}
			}

			http.Redirect(w, r, s.SpotifyAuth.AuthURL("solid"), http.StatusMovedPermanently)
			return
		}

		// From Spotify
		token, err := s.SpotifyAuth.Token("solid", r)
		if err != nil {
			http.Error(w, "Couldn't get token", http.StatusNotFound)
			return
		}

		// Add to DB and assign ID
		t := Token{Token: code, State: "solid"}
		s.DB.Create(&t)

		client := s.SpotifyAuth.NewClient(token)
		s.SpotifyApi[int(t.ID)] = client

		_, jwtToken, _ := s.JWTAuth.Encode(map[string]interface{}{"user_id": t.ID})
		cookie := http.Cookie{
			Name:  "jwt",
			Value: jwtToken,
			Path:  "/",
		}

		http.SetCookie(w, &cookie)
		http.Redirect(w, r, os.Getenv("APP_URL"), http.StatusMovedPermanently)
	})

	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(s.JWTAuth))
		r.Use(jwtauth.Authenticator)
		r.Use(s.APIContext)

		r.Get("/play", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			uid, _ := r.Context().Value("uid").(int)
			err := s.DoPlay(uid)
			if err != nil {
				w.WriteHeader(http.StatusForbidden)
				json.NewEncoder(w).Encode(JSONError{Error: err.Error()})
				return
			}

			json.NewEncoder(w).Encode(JSONStatus{Status: "ok"})
		})

		r.Get("/pause", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			uid, _ := r.Context().Value("uid").(int)
			err := s.DoPause(uid)
			if err != nil {
				w.WriteHeader(http.StatusForbidden)
				json.NewEncoder(w).Encode(JSONError{Error: err.Error()})
				return
			}

			json.NewEncoder(w).Encode(JSONStatus{Status: "ok"})
		})
	})

	return r
}
